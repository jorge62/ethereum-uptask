// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;
pragma experimental ABIEncoderV2;

contract ToDo {
  struct Task {
    uint id;
    uint date;
    string content;
    string author;
    bool done;
    uint dateComplete;
  }

  uint lastTaskId = 0;
  mapping(uint => Task) tasks;

  event TaskCreated(
    uint id,
    uint date,
    string content,
    string author,
    bool done
  );

  event TaskStatusToggled(
    uint id, 
    bool done, 
    uint date
  );

  function createTask(
    string calldata _content, 
    string calldata _author) 
    external {
    lastTaskId++;
    tasks[lastTaskId] = Task(lastTaskId, block.timestamp, _content, _author, false, 0);
    emit TaskCreated(lastTaskId, block.timestamp, _content, _author, false);
  }

  function getTasks()
    external
    view
    returns(Task[] memory) {
    Task[] memory _tasks = new Task[](lastTaskId);
    for(uint i = 0; i < lastTaskId; i++) {
      _tasks[i] = tasks[i+1];
    }
    return _tasks;
  }

  function toggleDone(uint id) 
    external
    taskExists(id) {
    Task storage task = tasks[id];
    task.done = !task.done;
    task.dateComplete = task.done ? block.timestamp : 0;
    emit TaskStatusToggled(id, task.done, task.dateComplete);
  }

  modifier taskExists(uint id) {
    require(tasks[id].id != 0);
    _;
  }
}