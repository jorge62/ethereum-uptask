import React, { Component, Fragment } from 'react';
import Header from './Header';
import Footer from './Footer';
import NewTask from './NewTask';
import Tasks from './Tasks';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accounts: [],
      address: null,
      tasks: [],
      balance: null
    };
    this.getTasks = this.getTasks.bind(this);
    this.createTask = this.createTask.bind(this);
    this.toggleDone = this.toggleDone.bind(this);
  }

  async getTasks() {
    const { todo } = this.props;
    return await todo.methods.getTasks().call();
  }

  async createTask(content, author) {
    const { web3, todo } = this.props;
    const receipt = await todo.methods
      .createTask(content, author)
      .send({
        from: this.state.accounts[5],
        gas: 1000000
      });
    console.log(receipt);
    const tasks = await this.getTasks();
    let balance = await web3.eth.getBalance(this.state.accounts[5])
    balance = web3.utils.fromWei(balance,"ether"); 
    this.setState({tasks,balance});
  }

  async toggleDone(id) {
    const { web3,todo } = this.props;
    const receipt = await todo.methods
      .toggleDone(id)
      .send({
        from: this.state.accounts[5],
        gas: 1000000
      });
    const tasks = await this.getTasks();
    let balance = await web3.eth.getBalance(this.state.accounts[5])
    balance = web3.utils.fromWei(balance,"ether"); 
    this.setState({tasks,balance});
  }

  async componentDidMount(){
    const { web3, todo } = this.props;
    const accounts = await web3.eth.getAccounts();
    let balance = await web3.eth.getBalance(accounts[5])
    balance = web3.utils.fromWei(balance,"ether");
    const tasks = await this.getTasks();
    this.setState({ 
      accounts, 
      address: todo.options.address, 
      tasks,
      balance 
    });
  }

  render() {
    const { accounts, address, tasks, balance } = this.state;
    if(accounts.length === 0) return <div>Loading...</div>;
    return (
      <Fragment>
        <Header address={address} />
        <div className="lateral col-sm-12">
          <div className="row">
            <h3>My account in metamask</h3>
           <div className="col-sm-8">
           <h4>{accounts[5]}</h4>
           </div> 
          </div>
          <div className="row">
            <h3>Balance of</h3>
            <div className="col-sm-12">
             <h4>{balance}  Ether</h4>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-sm-10">
              <NewTask createTask={this.createTask} />
            </div>
          </div>
          <br></br>
          <div className="row">
            <div className="col-sm-10">
              <Tasks tasks={tasks} toggleDone={this.toggleDone} />
            </div>
          </div>
        </div>
        <Footer/>
      </Fragment>
    );
  }
}

export default App;
