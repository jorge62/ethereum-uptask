import React from 'react';
import "../../src/App.css";

const Header = ({address}) => {
  return (
    <div id="page-header" className="row">
      <div className="col-sm-12">
        <h1 className="text-center title">Ethereum Task Creator<br />
          <span className="address">Address Contract: {address}</span>
        </h1>
      </div>
    </div>
  );
};

export default Header;
