import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import eth from './getWeb3';

ReactDOM.render(<App web3={eth.web3} todo={eth.todo}/>, document.getElementById('app'));





