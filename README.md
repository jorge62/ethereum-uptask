# Ethereum Uptask


## Description
This dapp describes the operation of the blockchain network, creating tasks and finishing them using metamask


## Installation & deploy smart contract
    #Download the project & install dependencies
    git clone https://gitlab.com/jorge62/ethereum-uptask.git
    cd /ethereum-uptask/client
    npm install
    #Start truffle suite
    go to => /ethereum-uptask
    truffle develop
    migrate 

## Usage
    #Once the contract is migrated, truffle generates a couple of accounts with a Mnemonic, 
    which can be used in metamask to keep track of our account

## Authors and acknowledgment
Thanks for the knowledge given Julian Keplath.

## License
    #GNU GENERAL PUBLIC LICENSE v3.0.0

## Project status
Finalized

